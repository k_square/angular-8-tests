import { Component, OnInit, forwardRef } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  NG_VALUE_ACCESSOR,
  ControlValueAccessor,
  NG_VALIDATORS,
  Validator,
  AbstractControl,
  ValidationErrors
} from '@angular/forms';

@Component({
  selector: 'app-nested-form',
  templateUrl: './nested-form.component.html',
  styleUrls: ['./nested-form.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NestedFormComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => NestedFormComponent),
      multi: true
    }
  ]
})
export class NestedFormComponent implements OnInit, ControlValueAccessor, Validator {

  adresseForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.adresseForm = this.fb.group({
      rue: ['', Validators.required],
      ville: ['', Validators.required],
      code_postale: ['', [Validators.required, Validators.maxLength(5)]]
    });
  }

  public onTouched: () => void = () => { };

  writeValue(val: any): void {
    val && this.adresseForm.setValue(val, { emitEvent: false });
  }
  registerOnChange(fn: any): void {
    this.adresseForm.valueChanges.subscribe(fn);
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.adresseForm.disable() : this.adresseForm.enable();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.adresseForm.valid ? null : { invalidForm: { valid: false, message: 'adresse form is invalid' } };
  }

}
