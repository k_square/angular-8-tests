import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsTestRoutingModule } from './forms-test-routing.module';
import { FormsTestComponent } from './forms-test.component';
import { HostFormComponent } from './host-form/host-form.component';
import { NestedFormComponent } from './nested-form/nested-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [FormsTestComponent, HostFormComponent, NestedFormComponent],
  imports: [
    FormsTestRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    MaterialModule
  ]
})
export class FormsTestModule { }
