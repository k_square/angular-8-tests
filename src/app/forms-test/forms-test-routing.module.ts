import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsTestComponent } from './forms-test.component';

const routes: Routes = [
  { path: '', component: FormsTestComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsTestRoutingModule { }
