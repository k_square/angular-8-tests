import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-host-form',
  templateUrl: './host-form.component.html',
  styleUrls: ['./host-form.component.css']
})
export class HostFormComponent implements OnInit {

  dataform: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.dataform = this.fb.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      adresse: new FormControl('', Validators.required)
    });
  }

}
