import { TestBed } from '@angular/core/testing';

import { PostsService } from './posts.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Post } from '../model/posts.model';

describe('PostsService', () => {
  let httpTestingController: HttpTestingController;
  let postsService: PostsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    postsService = TestBed.get(PostsService);
  });

  it('should be created', () => {
    const service: PostsService = TestBed.get(PostsService);
    expect(service).toBeTruthy();
  });

  it('should get posts correctly', done => {
    postsService.getPosts().subscribe(postsData => {
      expect(postsData[0].title).toEqual('Long time since');
      done();
    });

    const req = httpTestingController.expectOne('https://jsonplaceholder.typicode.com/posts');
    req.flush(apiPosts);
  });
});


const apiPosts: Post[] = [
  {
    id: 1,
    body: 'It\'s been a while',
    title: 'Long time since',
    userId: 5
  }
];
