import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'marbles',
    loadChildren: () => import('./marbles/marbles.module').then(mod => mod.MarblesModule)
  },
  {
    path: 'forms-test',
    loadChildren: () => import('./forms-test/forms-test.module').then(mod => mod.FormsTestModule)
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
