import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { cold } from 'jest-marbles';
import { UsersListComponent } from './users-list.component';
import { USER_FACADE } from 'src/app/facades/users-facade.interface';


describe('UsersListComponent', () => {
  let component: UsersListComponent;
  let fixture: ComponentFixture<UsersListComponent>;
  class UserFacadeMock {
    getUsers() {
      return cold('-b', {
        b: [
          { name: 'Leanne Graham', email: 'Sincere@april.biz', phone: '1-770-736-8031 x56442' },
          { name: 'Ervin Howell', email: 'Shanna@melissa.tv', phone: '010-692-6593 x09125' }
        ]
      });
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsersListComponent],
      providers: [
        { provide: USER_FACADE, useClass: UserFacadeMock}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show elements', () => {
    // const el = fixture.debugElement.queryAll(By.css('.user'));
    // expect(el.length).toBe(2);
    (expect(component.usersBasicInfo$) as any).toBeObservable(cold('-b', {b: [
      { name: 'Leanne Graham', email: 'Sincere@april.biz', phone: '1-770-736-8031 x56442' },
      { name: 'Ervin Howell', email: 'Shanna@melissa.tv', phone: '010-692-6593 x09125' }
    ]}));
  });
});
