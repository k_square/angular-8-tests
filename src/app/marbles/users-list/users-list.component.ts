import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserBasicInfo } from 'src/app/model/users.model';
import { UserFacadeImpl } from 'src/app/facades/users-facade.impl';
import { USER_FACADE } from 'src/app/facades/users-facade.interface';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  usersBasicInfo$: Observable<UserBasicInfo[]>;

  constructor(@Inject(USER_FACADE) private userFacade: UserFacadeImpl) { }

  ngOnInit() {
    this.usersBasicInfo$ = this.userFacade.getUsers();
  }

}
