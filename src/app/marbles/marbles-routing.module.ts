import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarblesComponent } from './marbles.component';

const routes: Routes = [
  {path: '', component: MarblesComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MarblesRoutingModule { }
