import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarblesComponent } from './marbles.component';
import { UsersListComponent } from './users-list/users-list.component';
import { PostsListComponent } from './posts-list/posts-list.component';
import { MarblesRoutingModule } from './marbles-routing.module';

@NgModule({
  declarations: [
    MarblesComponent,
    UsersListComponent,
    PostsListComponent
  ],
  imports: [
    MarblesRoutingModule,
    CommonModule
  ]
})
export class MarblesModule { }
