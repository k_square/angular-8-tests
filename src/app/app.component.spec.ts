import { async, TestBed } from '@angular/core/testing';
import { cold, hot } from 'jest-marbles';
import { concat, merge } from 'rxjs/operators';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'angular8-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('angular8-app');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('angular8-app');
  });

  it('Should concatenate two cold observables into single cold observable', () => {
    const a = cold('-a-|');
    const b = cold('-b-|');
    const expected = '-a--b-|';
    (expect(a.pipe(concat(b))) as any).toBeMarble(expected);
  });

  it('Should merge two hot observables and start emitting from the subscription point', () => {
    const e1 = hot('----a--^--b-------c--|', { a: 0 });
    const e2 = hot('  ---d-^--e---------f-----|', { a: 0 });
    const expected = cold('---(be)----c-f-----|', { a: 0 });

    (expect(e1.pipe(merge(e2))) as any).toBeObservable(expected);
  });

  it('Should transform one observable to another', () => {

  });
});
