import { map } from 'rxjs/operators';
import { UsersService } from '../services/users.service';
import { UsersFacadeInterface } from './users-facade.interface';
import { Injectable } from '@angular/core';

@Injectable()
export class UserFacadeImpl implements UsersFacadeInterface {

  constructor(private usersService: UsersService) {

  }

  getUsers() {
    return this.usersService.getUsers().pipe(
      map(users => users.map(user => ({ name: user.name, email: user.email, phone: user.phone })))
    );
  }

}
