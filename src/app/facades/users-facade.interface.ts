import { Observable } from 'rxjs';
import { UserBasicInfo } from '../model/users.model';
import { InjectionToken } from '@angular/core';

export interface UsersFacadeInterface {
  getUsers: () => Observable<UserBasicInfo[]>;
}

export const USER_FACADE = new InjectionToken<UsersFacadeInterface>('USER_FACADE');
