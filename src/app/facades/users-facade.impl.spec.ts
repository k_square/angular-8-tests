import { TestBed } from '@angular/core/testing';
import { cold } from 'jest-marbles';
import { User } from '../model/users.model';
import { UsersService } from '../services/users.service';
import { UserFacadeImpl } from './users-facade.impl';
import { USER_FACADE } from './users-facade.interface';

describe('UserFacadeImpl', () => {
    let userFacadeImpl: UserFacadeImpl;


    const apiUsers: User[] = [
        {
            id: 1,
            name: 'Leanne Graham',
            username: 'Bret',
            email: 'Sincere@april.biz',
            address: {
                street: 'Kulas Light',
                suite: 'Apt. 556',
                city: 'Gwenborough',
                zipcode: '92998-3874',
                geo: {
                    lat: '-37.3159',
                    lng: '81.1496'
                }
            },
            phone: '1-770-736-8031 x56442',
            website: 'hildegard.org',
            company: {
                name: 'Romaguera-Crona',
                catchPhrase: 'Multi-layered client-server neural-net',
                bs: 'harness real-time e-markets'
            }
        },
        {
            id: 2,
            name: 'Ervin Howell',
            username: 'Antonette',
            email: 'Shanna@melissa.tv',
            address: {
                street: 'Victor Plains',
                suite: 'Suite 879',
                city: 'Wisokyburgh',
                zipcode: '90566-7771',
                geo: {
                    lat: '-43.9509',
                    lng: '-34.4618'
                }
            },
            phone: '010-692-6593 x09125',
            website: 'anastasia.net',
            company: {
                name: 'Deckow-Crist',
                catchPhrase: 'Proactive didactic contingency',
                bs: 'synergize scalable supply-chains'
            }
        }
    ];

    beforeEach(() => {
        const usersApi$ = cold('-(a|)', { a: apiUsers });
        const userServiceMock = {
            getUsers: jest.fn(() => usersApi$)
        };
        TestBed.configureTestingModule({
            providers: [
                { provide: UsersService, useValue: userServiceMock },
                UserFacadeImpl
            ]
        });
        userFacadeImpl = TestBed.get(UserFacadeImpl);
    });

    it('should create user facade impl', () => {
        expect(userFacadeImpl).toBeTruthy();
    });

    it('should get the right observable in output', () => {
        const userTransformed = [
            { name: 'Leanne Graham', email: 'Sincere@april.biz', phone: '1-770-736-8031 x56442' },
            { name: 'Ervin Howell', email: 'Shanna@melissa.tv', phone: '010-692-6593 x09125' }
        ];
        const obsResult$ = cold('-(b|)', { b: userTransformed });
        (expect(userFacadeImpl.getUsers()) as any).toBeObservable(obsResult$);
    });

});

